grammar JGS;

@header {
	package ee.ut.cs.ojandu.jgensearch.parser;
}

jgs
	:  (packageDef)? algoDef entityDef propDefs evalDefs genDefs
	;
	
packageDef
	: 'package' '=' PackageName ';'
	;
	
algoDef
	: 'name' '=' Name ';'
	;
	
entityDef
	: 'entity'  '=' Name ';'
	;

propDefs
	: 'Props' ':' (Name)* ';'
	;
	
evalDefs
	: 'Evals' ':' (Name)+ ';'
	;
	
genDefs
	: 'Gens' ':' (Name)+ ';'
	;
	
Name
	: [a-zA-Z][a-zA-Z0-9]*
	;
	
PackageName
	: [a-zA-Z][a-zA-Z0-9_]*([\.][a-zA-Z_][a-zA-Z0-9_]*)*
	;
	
WS  
	:  [ \t\r\n\u000C]+ -> skip
    ;