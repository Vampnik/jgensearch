package ee.ut.cs.ojandu.jgensearch;

import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Repeat<E extends Entity> implements Statement<E> {

	private static Logger log = LoggerFactory.getLogger(Repeat.class);

	private int times;

	private Statement<E>[] stmts;

	private static final float logRatio = 0.1f;

	public Repeat(int times, Statement<E>[] stmts) {
		this.times = times;
		this.stmts = stmts;
	}

	@Override
	public void call(Set<E> set) {
		int logAt = (int) (times * logRatio);
		for (int i = 0; i < times; i++) {
			if (logAt != 0 && (i+1) % logAt == 0) {
				log.info("Repeating: {}/{}", i+1, times);
			}
			for (Statement<E> s : stmts) {
				s.call(set);
			}
		}
	}
}
