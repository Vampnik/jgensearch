package ee.ut.cs.ojandu.jgensearch;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import ee.ut.cs.ojandu.jgensearch.util.JGSUtil;

public class AllPairs<E extends Entity> extends SetSplitter<E> {

	public AllPairs(Statement<E>[] stmts) {
		super(stmts);
	}

	@SuppressWarnings("unchecked")
	@Override
	public Set<E>[] split(Set<E> set) {
		final E[] ar = JGSUtil.setToArray(set);

		Set<E>[] res = (Set<E>[]) new Set[ar.length * (1 + ar.length) / 2];
		
		int index = 0;
		for(int i=0;i<ar.length;i++) {
			for(int j=i;j<ar.length;j++) {
				Set<E> sub = new HashSet<E>();
				sub.add(ar[i]);
				sub.add(ar[j]);
				res[index++] = sub;
			}
		}
		return res;
	}

}
