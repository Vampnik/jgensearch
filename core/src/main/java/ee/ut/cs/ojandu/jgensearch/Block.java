package ee.ut.cs.ojandu.jgensearch;

import java.util.Set;

public class Block<E extends Entity> implements Statement<E>{

	private Statement<E>[] stmts;
	
	public Block(Statement<E>[] stmts) {
		this.stmts = stmts;
	}

	@Override
	public void call(Set<E> set) {
		for(Statement<E> s : stmts) {
			s.call(set);
		}
	}
}
