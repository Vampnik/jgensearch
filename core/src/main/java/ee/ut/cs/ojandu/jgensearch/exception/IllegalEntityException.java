package ee.ut.cs.ojandu.jgensearch.exception;

public class IllegalEntityException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public IllegalEntityException() {
		super();
	}

	public IllegalEntityException(String message, Throwable cause) {
		super(message, cause);
	}

	public IllegalEntityException(String message) {
		super(message);
	}

	public IllegalEntityException(Throwable cause) {
		super(cause);
	}

}
