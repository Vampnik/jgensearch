package ee.ut.cs.ojandu.jgensearch;

import java.util.HashSet;
import java.util.Set;

public class Each<E extends Entity> extends SetSplitter<E> {

	public Each(Statement<E>[] stmts) {
		super(stmts);
	}

	@SuppressWarnings("unchecked")
	@Override
	public Set<E>[] split(Set<E> set) {
		Set<E>[] res = new HashSet[set.size()];
		int i = 0;
		for (final E e : set) {
			res[i++] = new HashSet<E>() {
				private static final long serialVersionUID = 1L;

				{
					add(e);
				}
			};
		}
		return res;
	}

}
