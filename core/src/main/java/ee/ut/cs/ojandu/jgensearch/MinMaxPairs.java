package ee.ut.cs.ojandu.jgensearch;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import ee.ut.cs.ojandu.jgensearch.util.JGSUtil;

public class MinMaxPairs<E extends Entity> extends SetSplitter<E> {

	public MinMaxPairs(Statement<E>[] stmts) {
		super(stmts);
	}

	@SuppressWarnings("unchecked")
	@Override
	public Set<E>[] split(Set<E> set) {
		final E[] ar = JGSUtil.setToArray(set);
		Arrays.sort(ar);
		int top = ar.length - 1;
		int bottom = 0;

		int len = ar.length;
		Set<E>[] res = (Set<E>[]) new Set[len / 2 + (len % 2)];
		int index = 0;
		while (bottom <= top) {
			Set<E> sub = new HashSet<E>();
			sub.add(ar[bottom]);
			sub.add(ar[top]);
			res[index++] = sub;
			bottom++;
			top--;
		}
		return res;
	}

}
