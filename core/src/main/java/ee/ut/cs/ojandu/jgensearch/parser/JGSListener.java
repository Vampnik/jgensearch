// Generated from JGS.g4 by ANTLR 4.2

	package ee.ut.cs.ojandu.jgensearch.parser;

import org.antlr.v4.runtime.misc.NotNull;
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link JGSParser}.
 */
public interface JGSListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link JGSParser#algoDef}.
	 * @param ctx the parse tree
	 */
	void enterAlgoDef(@NotNull JGSParser.AlgoDefContext ctx);
	/**
	 * Exit a parse tree produced by {@link JGSParser#algoDef}.
	 * @param ctx the parse tree
	 */
	void exitAlgoDef(@NotNull JGSParser.AlgoDefContext ctx);

	/**
	 * Enter a parse tree produced by {@link JGSParser#entityDef}.
	 * @param ctx the parse tree
	 */
	void enterEntityDef(@NotNull JGSParser.EntityDefContext ctx);
	/**
	 * Exit a parse tree produced by {@link JGSParser#entityDef}.
	 * @param ctx the parse tree
	 */
	void exitEntityDef(@NotNull JGSParser.EntityDefContext ctx);

	/**
	 * Enter a parse tree produced by {@link JGSParser#packageDef}.
	 * @param ctx the parse tree
	 */
	void enterPackageDef(@NotNull JGSParser.PackageDefContext ctx);
	/**
	 * Exit a parse tree produced by {@link JGSParser#packageDef}.
	 * @param ctx the parse tree
	 */
	void exitPackageDef(@NotNull JGSParser.PackageDefContext ctx);

	/**
	 * Enter a parse tree produced by {@link JGSParser#propDefs}.
	 * @param ctx the parse tree
	 */
	void enterPropDefs(@NotNull JGSParser.PropDefsContext ctx);
	/**
	 * Exit a parse tree produced by {@link JGSParser#propDefs}.
	 * @param ctx the parse tree
	 */
	void exitPropDefs(@NotNull JGSParser.PropDefsContext ctx);

	/**
	 * Enter a parse tree produced by {@link JGSParser#jgs}.
	 * @param ctx the parse tree
	 */
	void enterJgs(@NotNull JGSParser.JgsContext ctx);
	/**
	 * Exit a parse tree produced by {@link JGSParser#jgs}.
	 * @param ctx the parse tree
	 */
	void exitJgs(@NotNull JGSParser.JgsContext ctx);

	/**
	 * Enter a parse tree produced by {@link JGSParser#evalDefs}.
	 * @param ctx the parse tree
	 */
	void enterEvalDefs(@NotNull JGSParser.EvalDefsContext ctx);
	/**
	 * Exit a parse tree produced by {@link JGSParser#evalDefs}.
	 * @param ctx the parse tree
	 */
	void exitEvalDefs(@NotNull JGSParser.EvalDefsContext ctx);

	/**
	 * Enter a parse tree produced by {@link JGSParser#genDefs}.
	 * @param ctx the parse tree
	 */
	void enterGenDefs(@NotNull JGSParser.GenDefsContext ctx);
	/**
	 * Exit a parse tree produced by {@link JGSParser#genDefs}.
	 * @param ctx the parse tree
	 */
	void exitGenDefs(@NotNull JGSParser.GenDefsContext ctx);
}