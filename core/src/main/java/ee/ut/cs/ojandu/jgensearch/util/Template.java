package ee.ut.cs.ojandu.jgensearch.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;

import ee.ut.cs.ojandu.jgensearch.parser.JGSPlaceholders;

public class Template {
	
	private String result = "";

	public Template(String templateLocation) throws IOException {
		StringBuilder sb = new StringBuilder();
		InputStream in = this.getClass().getClassLoader()
				.getResourceAsStream(templateLocation);
		BufferedReader fin = new BufferedReader(new InputStreamReader(in));
		String line = fin.readLine();
		while(line != null) {
			sb.append(line).append("\n");
			line = fin.readLine();
		}
		in.close();
		this.result = sb.toString();
	}
	
	public void swapPlaceholder(JGSPlaceholders placeholder, Object value) {
		result = result.replaceAll("\\$" + placeholder.name() + "\\$", value.toString());
	}
	
	public void writeToFile(String fileName) throws FileNotFoundException {
		PrintWriter fout = new PrintWriter(new File(fileName));
		fout.println(result);
		fout.close();
	}
	
	@Override
	public String toString() {
		return result;
	}
}
