package ee.ut.cs.ojandu.jgensearch.util;

import java.util.Set;
import java.util.regex.Pattern;

import ee.ut.cs.ojandu.jgensearch.Entity;
import ee.ut.cs.ojandu.jgensearch.exception.IllegalEntityException;

public class JGSUtil {
	
	public static final String JGS_ALG_FILE_NAME = ".jgs-alg";
	
	public static final String PROPS_FILE = "jgs.properties";
	
	public static final String POM_TEMPLATE = "templates/pom.t";

	public static final String ENTITY_TEMPLATE = "templates/entity.t";
	
	public static final String GENS_TEMPLATE = "templates/gen.t";
	
	public static final String EVALS_TEMPLATE = "templates/eval.t";
	
	public static final String ALGORITHM_TEMPLATE = "templates/algorithm.t";
	
	public static final String LOG4J_TEMPLATE = "templates/log4j.t";
	
	public static final String getJGSVersion() {
		return "1.0-SNAPSHOT";
	}
	
	@SuppressWarnings("unchecked")
	public static <E extends Entity> E[] setToArray(Set<E> set) {
		E[] res = (E[]) new Entity[set.size()];
		int index = 0;
		for (E e : set) {
			res[index++] = e;
		}
		return res;
	}
	
	public static <E extends Entity> E getStubEntity(Class<E> entityClass) {
		try {
			return entityClass.newInstance();
		} catch (InstantiationException e) {
			throw new IllegalEntityException(
					"Could not construct an instacne of entity. Make sure your Entity has a public default constructor.");
		} catch (IllegalAccessException e) {
			throw new IllegalEntityException(
					"Could not construct an instacne of entity. Make sure your Entity has a public default constructor.");
		}
	}
	
	public static String camelToDash(String camel) {
		String regex = "([a-z])([A-Z])";
        String replacement = "$1-$2";
        return camel.replaceAll(regex, replacement).toLowerCase();
	}
	
	public static String[] splitPackage(String packageString) {
		return packageString.split("\\.");
	}
	
	public static String packageToPath(String packageString) {
		return packageString.replaceAll("\\.", "/");
	}
}
