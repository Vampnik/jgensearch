package ee.ut.cs.ojandu.jgensearch.parser;

public enum JGSPlaceholders {
	JGS_VERSION,
	GROUP_ID,
	ARTIFACT_ID,
	PACKAGE,
	ENTITY_NAME,
	GENS,
	EVALS,
	GEN_NAME,
	EVAL_NAME,
	PROPS_FILE,
	ALGORITHM_NAME
}
