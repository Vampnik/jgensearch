package ee.ut.cs.ojandu.jgensearch.parser;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;

public class JGSInit {
	public static void main(String[] args) {
		if(args.length == 0) {
			usage();
			return;
		}
		
		String jgs;
		try {
			jgs = readJgsFile(args[0]);
		} catch (IOException e) {
			System.out.println("Unable to read file: " + args[0]);
			return;
		}
		
		ANTLRInputStream antlrInput = new ANTLRInputStream(jgs);
		JGSLexer lexer = new JGSLexer(antlrInput);
		CommonTokenStream tokens = new CommonTokenStream(lexer);
		JGSParser parser = new JGSParser(tokens);
		parser.addParseListener(new JGSListenerImpl());
		parser.jgs();
	}

	private static String readJgsFile(String name) throws IOException {
		StringBuilder sb = new StringBuilder();
		BufferedReader fin = new BufferedReader(new FileReader(name));
		String line = fin.readLine();
		while(line != null) {
			sb.append(line).append("\n");
			line = fin.readLine();
		}
		fin.close();
		return sb.toString();
	}

	private static void usage() {
		System.out.println("jgs-init [JGS FILE NAME]");
	}
}
