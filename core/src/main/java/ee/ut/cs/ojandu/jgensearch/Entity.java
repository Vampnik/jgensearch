package ee.ut.cs.ojandu.jgensearch;

public abstract class Entity implements Comparable<Entity> {

	private Double value;
	
	public abstract <E extends Entity> E initGenerate();

	public Double getEvalValue() {
		return value;
	}
	
	public void evaluate(double value) {
		this.value = value;
	}

	@Override
	public int compareTo(Entity o) {
		if (value == null && o.value == null) {
			return 0;
		} else if (value != null && o.value == null) {
			return 1;
		} else if (value == null && o.value != null) {
			return -1;
		} else {
			return value.compareTo(o.value);
		}
	}

}
