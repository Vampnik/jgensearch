package ee.ut.cs.ojandu.jgensearch.parser;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.LinkedList;
import java.util.List;

import org.antlr.v4.runtime.misc.NotNull;
import org.antlr.v4.runtime.tree.ErrorNode;
import org.antlr.v4.runtime.tree.TerminalNode;

import ee.ut.cs.ojandu.jgensearch.parser.JGSBaseListener;
import ee.ut.cs.ojandu.jgensearch.parser.JGSParser;
import ee.ut.cs.ojandu.jgensearch.util.JGSUtil;
import ee.ut.cs.ojandu.jgensearch.util.Template;

public class JGSListenerImpl extends JGSBaseListener {

	private String packageName = null;

	private String algorithm;

	private String entity;

	private List<String> evals = new LinkedList<String>();

	private List<String> gens = new LinkedList<String>();

	private List<String> props = new LinkedList<String>();
	
	private boolean errorFlag = false;

	@Override
	public void exitEntityDef(@NotNull JGSParser.EntityDefContext ctx) {
		entity = ctx.Name().getText();
	}

	@Override
	public void exitPackageDef(@NotNull JGSParser.PackageDefContext ctx) {
		TerminalNode pn = ctx.PackageName();
		if(pn != null) {
			packageName = pn.getText();
		}
	}

	@Override
	public void exitAlgoDef(@NotNull JGSParser.AlgoDefContext ctx) {
		algorithm = ctx.Name().getText();
	}

	@Override
	public void exitEvalDefs(@NotNull JGSParser.EvalDefsContext ctx) {
		for (TerminalNode sn : ctx.Name()) {
			evals.add(sn.getText());
		}
	}

	@Override
	public void exitPropDefs(@NotNull JGSParser.PropDefsContext ctx) {
		for (TerminalNode sn : ctx.Name()) {
			props.add(sn.getText());
		}
	}

	@Override
	public void exitGenDefs(@NotNull JGSParser.GenDefsContext ctx) {
		for (TerminalNode sn : ctx.Name()) {
			gens.add(sn.getText());
		}
	}

	@Override
	public void visitErrorNode(@NotNull ErrorNode node) {
		errorFlag = true;
	}

	@Override
	public void exitJgs(@NotNull JGSParser.JgsContext ctx) {
		if(errorFlag) {
			return;
		}
		System.out.println("Starting to generate");
		try {
			makeFolders();
			writePom();
			writeEntity();
			writeAlgorithm();
			writePros();
			writeLog4j();
			writeJgsAlgFile();
		} catch (IOException e) {
			System.out.println("Could not initiate: " + e.getMessage());
			e.printStackTrace();
		}
		System.out.println("Finished generating");
	}

	private void writeJgsAlgFile() throws FileNotFoundException {
		PrintWriter fout = new PrintWriter(JGSUtil.JGS_ALG_FILE_NAME);
		String p = packageName == null ? "" : packageName + ".";
		fout.println(p + algorithm);
		fout.println(p + entity);
		fout.close();
	}

	private void writePros() throws FileNotFoundException {
		PrintWriter fout = new PrintWriter(JGSUtil.PROPS_FILE);
		for (String prop : props) {
			fout.println(prop + "=0");
		}
		fout.close();
	}

	private void writeLog4j() throws IOException {
		Template template = new Template(JGSUtil.LOG4J_TEMPLATE);
		template.writeToFile("src/main/resources/log4j.xml");
	}

	private void writeAlgorithm() throws IOException {
		String path = "src/main/java/";
		Template template = new Template(JGSUtil.ALGORITHM_TEMPLATE);

		if (packageName != null) {
			template.swapPlaceholder(JGSPlaceholders.PACKAGE, "package " + packageName + ";");
			path = path + JGSUtil.packageToPath(packageName) + "/";
		} else {
			template.swapPlaceholder(JGSPlaceholders.PACKAGE, "");
		}

		template.swapPlaceholder(JGSPlaceholders.ENTITY_NAME, entity);
		template.swapPlaceholder(JGSPlaceholders.ALGORITHM_NAME, algorithm);
		template.swapPlaceholder(JGSPlaceholders.PROPS_FILE, "\"" + JGSUtil.PROPS_FILE + "\"");

		template.writeToFile(path + algorithm + ".java");
	}

	private void writeEntity() throws IOException {
		String path = "src/main/java/";
		Template template = new Template(JGSUtil.ENTITY_TEMPLATE);

		if (packageName != null) {
			template.swapPlaceholder(JGSPlaceholders.PACKAGE, "package " + packageName + ";");
			path = path + JGSUtil.packageToPath(packageName) + "/";
		} else {
			template.swapPlaceholder(JGSPlaceholders.PACKAGE, "");
		}

		template.swapPlaceholder(JGSPlaceholders.ENTITY_NAME, entity);

		String gensResult = "";
		for (String gen : gens) {
			Template t = new Template(JGSUtil.GENS_TEMPLATE);
			t.swapPlaceholder(JGSPlaceholders.ENTITY_NAME, entity);
			t.swapPlaceholder(JGSPlaceholders.GEN_NAME, gen);
			gensResult = gensResult + "\n\n" + t;
		}
		template.swapPlaceholder(JGSPlaceholders.GENS, gensResult);

		String evalResult = "";
		for (String eval : evals) {
			Template t = new Template(JGSUtil.EVALS_TEMPLATE);
			t.swapPlaceholder(JGSPlaceholders.ENTITY_NAME, entity);
			t.swapPlaceholder(JGSPlaceholders.EVAL_NAME, eval);
			evalResult = evalResult + "\n\n" + t;
		}
		template.swapPlaceholder(JGSPlaceholders.EVALS, evalResult);

		template.writeToFile(path + entity + ".java");
	}

	private void writePom() throws IOException {
		Template pom = new Template(JGSUtil.POM_TEMPLATE);
		pom.swapPlaceholder(JGSPlaceholders.JGS_VERSION,
				JGSUtil.getJGSVersion());
		String artifactId = JGSUtil.camelToDash(algorithm);
		pom.swapPlaceholder(JGSPlaceholders.ARTIFACT_ID, artifactId);
		pom.swapPlaceholder(JGSPlaceholders.GROUP_ID,
				packageName == null ? artifactId : packageName);
		pom.writeToFile("pom.xml");
	}

	private void makeFolders() {
		String path = "src";
		new File(path).mkdir();
		path = path + "/main";
		new File(path).mkdir();
		new File(path + "/resources").mkdir();
		path = path + "/java";
		new File(path).mkdir();

		if (packageName != null) {
			for (String s : JGSUtil.splitPackage(packageName)) {
				path = path + "/" + s;
				new File(path).mkdir();
			}
		}
	}
}
