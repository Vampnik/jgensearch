package ee.ut.cs.ojandu.jgensearch;

import java.util.Set;

import ee.ut.cs.ojandu.jgensearch.util.JGSUtil;

public class InitialGenerate<E extends Entity> implements Statement<E> {
	
	private int num;
	
	private Class<E> entityClass;
	
	public InitialGenerate(int num) {
	}

	public InitialGenerate(Class<E> entityClass, int num) {
		this.entityClass = entityClass;
		this.num = num;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void call(Set<E> set) {
		E e = JGSUtil.getStubEntity(entityClass);
		for(int i=num;i!=0;i--) {
			set.add((E) e.initGenerate());
		}
	}

	
}
