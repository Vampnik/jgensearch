package ee.ut.cs.ojandu.jgensearch;

import java.util.Arrays;
import java.util.Set;

import ee.ut.cs.ojandu.jgensearch.util.JGSUtil;

public class KeepBottom<E extends Entity> implements Statement<E> {

	private int top;

	public KeepBottom(int top) {
		this.top = top;
	}

	@Override
	public void call(Set<E> set) {
		E[] ar = JGSUtil.setToArray(set);
		Arrays.sort(ar);
		set.clear();
		for (int i = 0; i < Math.min(top, ar.length); i++) {
			set.add(ar[i]);
		}
	}

}
