package ee.ut.cs.ojandu.jgensearch;

import java.util.HashSet;
import java.util.Set;

public abstract class SetSplitter<E extends Entity> implements Statement<E> {

	private Statement<E>[] stmts;

	public SetSplitter(Statement<E>[] stmts) {
		this.stmts = stmts;
	}

	@Override
	public void call(Set<E> set) {
		Set<E> tmp = new HashSet<E>(set);
		set.clear();
		for (Set<E> subSet : split(tmp)) {
			for (Statement<E> s : stmts) {
				s.call(subSet);
			}
			tmp.addAll(subSet);
		}
		set.addAll(tmp);
	}

	public abstract Set<E>[] split(Set<E> set);

}
