package ee.ut.cs.ojandu.jgensearch;

import java.util.Set;

public abstract class EntityEvaluator<E extends Entity> implements Statement<E> {
	public abstract void evaluate(Set<E> set);

	@Override
	public void call(Set<E> set) {
		evaluate(set);
	}

}
