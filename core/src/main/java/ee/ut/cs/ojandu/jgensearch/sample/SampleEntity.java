package ee.ut.cs.ojandu.jgensearch.sample;

import java.util.Set;

import ee.ut.cs.ojandu.jgensearch.Entity;
import ee.ut.cs.ojandu.jgensearch.EntityEvaluator;
import ee.ut.cs.ojandu.jgensearch.EntityGenerator;

public class SampleEntity extends Entity {
	
	private static long gen = 0;
	
	private long value = 0;
	
	public static final EntityGenerator<SampleEntity> SIMPLE_GENERATE = new EntityGenerator<SampleEntity>() {

		@Override
		public Set<SampleEntity> generate(Set<SampleEntity> set) {
			long newVal = 0;
			for(SampleEntity e : set) {
				newVal += e.value;
			}
			set.clear();
			SampleEntity ent = new SampleEntity();
			ent.value = newVal;
			set.add(ent);
			return set;
		}
	};
	
	public static final EntityEvaluator<SampleEntity> SIMPLE_EVALUATE = new EntityEvaluator<SampleEntity>() {
		
		@Override
		public void evaluate(Set<SampleEntity> set) {
			for(SampleEntity e : set) {
				e.evaluate(e.value);
			}
		}
	};

	@SuppressWarnings("unchecked")
	@Override
	public SampleEntity initGenerate() {
		SampleEntity ent = new SampleEntity();
		ent.value = ++SampleEntity.gen;
		return ent;
	}

	public long getValue() {
		return value;
	}

	public void setValue(long value) {
		this.value = value;
	}
	
}
