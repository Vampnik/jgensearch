package ee.ut.cs.ojandu.jgensearch;

import java.util.Arrays;
import java.util.Set;

import ee.ut.cs.ojandu.jgensearch.util.JGSUtil;

public class KeepTop<E extends Entity> implements Statement<E> {

	private int top;

	public KeepTop(int top) {
		this.top = top;
	}

	@Override
	public void call(Set<E> set) {
		E[] ar = JGSUtil.setToArray(set);
		Arrays.sort(ar);
		set.clear();
		for(int i=Math.min(top, ar.length)-1;i>=0;i--) {
			set.add(ar[i]);
		}
	}

	
}
