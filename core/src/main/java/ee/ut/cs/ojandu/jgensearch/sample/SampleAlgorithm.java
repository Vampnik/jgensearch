package ee.ut.cs.ojandu.jgensearch.sample;

import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ee.ut.cs.ojandu.jgensearch.Algorithm;
import ee.ut.cs.ojandu.jgensearch.Statement;

public class SampleAlgorithm extends Algorithm<SampleEntity> {

	private static Logger log = LoggerFactory.getLogger(SampleEntity.class);
	
	@SuppressWarnings({ "unchecked", "static-access" })
	@Override
	public Statement<SampleEntity> root() {
		return block(
				initialGenerate(prop("initNum")),
				repeat(prop("totalRepeat"), 
						each(
								entity().SIMPLE_EVALUATE
						),
						keepTop(prop("topToKeep")),
						minMaxPairs(
								entity().SIMPLE_GENERATE
						)
				)
		);
	}
	
	public static void main(String[] args) {
		run(SampleAlgorithm.class, SampleEntity.class, "sample.properties");
		
	}

	@Override
	public void processResult(Set<SampleEntity> result) {
		log.info("Results");
		for(SampleEntity e :result) {
			log.info(" : " + e.getValue());
		}
	}

}
