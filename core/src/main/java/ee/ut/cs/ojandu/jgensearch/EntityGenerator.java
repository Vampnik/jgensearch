package ee.ut.cs.ojandu.jgensearch;

import java.util.HashSet;
import java.util.Set;

public abstract class EntityGenerator<E extends Entity> implements
		Statement<E> {

	public abstract Set<E> generate(Set<E> set);

	@SuppressWarnings("unchecked")
	@Override
	public void call(Set<E> set) {
		Set<E> tmp = (Set<E>) new HashSet<Entity>(set);
		set.clear();
		set.addAll(generate(tmp));
	}

}
