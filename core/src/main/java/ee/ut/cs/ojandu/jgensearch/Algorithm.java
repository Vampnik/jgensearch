package ee.ut.cs.ojandu.jgensearch;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashSet;
import java.util.Properties;
import java.util.Set;

import ee.ut.cs.ojandu.jgensearch.util.JGSUtil;

public abstract class Algorithm<E extends Entity> {

	private Class<E> entityClass;

	private Properties props = new Properties();

	public abstract Statement<E> root();
	
	public abstract void processResult(Set<E> result);

	@SuppressWarnings("unchecked")
	public Block<E> block(Statement<E>... stmts) {
		return new Block<E>(stmts);
	}

	@SuppressWarnings("unchecked")
	public Repeat<E> repeat(int times, Statement<E>... stmts) {
		return new Repeat<E>(times, stmts);
	}

	@SuppressWarnings("unchecked")
	public Each<E> each(Statement<E>... stmts) {
		return new Each<E>(stmts);
	}

	public InitialGenerate<E> initialGenerate(int num) {
		return new InitialGenerate<E>(entityClass, num);
	}

	@SuppressWarnings("unchecked")
	public MinMaxPairs<E> minMaxPairs(Statement<E>... stmts) {
		return new MinMaxPairs<E>(stmts);
	}

	@SuppressWarnings("unchecked")
	public AllPairs<E> allPairs(Statement<E>... stmts) {
		return new AllPairs<E>(stmts);
	}

	public KeepTop<E> keepTop(int num) {
		return new KeepTop<E>(num);
	}

	public KeepBottom<E> keepBottom(int num) {
		return new KeepBottom<E>(num);
	}

	public E entity() {
		return JGSUtil.getStubEntity(entityClass);
	}

	public int prop(String key) {
		try {
			return Integer.parseInt((String) props.get(key));
		} catch (NumberFormatException e) {
			throw new IllegalArgumentException("Cannot convert property " + key + " into a integer");
		}
	}

	public Set<E> emptySet() {
		return new HashSet<E>();
	}

	void setEntityClass(Class<E> entityClass) {
		this.entityClass = entityClass;
	}
	
	void loadProperties(String fileName) {
		try {
			props.load(new FileReader(fileName));
		} catch (FileNotFoundException e) {
			throw new IllegalArgumentException("Could not find properties file: " + fileName);
		} catch (IOException e) {
			throw new IllegalArgumentException("Could not read properties file: " + fileName);
		}
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static <A extends Algorithm<E>, E extends Entity> void run(
			Class algorithmClass, Class entityClass, String propertiesFile) {
		A alg;
		try {
			alg = (A) algorithmClass.newInstance();
		} catch (InstantiationException e) {
			throw new IllegalArgumentException(
					"Coult not instansiate algorithm " + algorithmClass
							+ ". Make sure default constructor is present.");
		} catch (IllegalAccessException e) {
			throw new IllegalArgumentException(
					"Could not acces default constructor for " + algorithmClass
							+ ". Make sure its public!");
		}
		alg.setEntityClass(entityClass);
		alg.loadProperties(propertiesFile);
		HashSet<E> set = new HashSet<E>();
		alg.root().call(set);
		alg.processResult(set);
	}

}
