// Generated from JGS.g4 by ANTLR 4.2

	package ee.ut.cs.ojandu.jgensearch.parser;

import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class JGSParser extends Parser {
	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__8=1, T__7=2, T__6=3, T__5=4, T__4=5, T__3=6, T__2=7, T__1=8, T__0=9, 
		Name=10, PackageName=11, WS=12;
	public static final String[] tokenNames = {
		"<INVALID>", "'package'", "'Gens'", "'name'", "'Evals'", "':'", "'='", 
		"';'", "'entity'", "'Props'", "Name", "PackageName", "WS"
	};
	public static final int
		RULE_jgs = 0, RULE_packageDef = 1, RULE_algoDef = 2, RULE_entityDef = 3, 
		RULE_propDefs = 4, RULE_evalDefs = 5, RULE_genDefs = 6;
	public static final String[] ruleNames = {
		"jgs", "packageDef", "algoDef", "entityDef", "propDefs", "evalDefs", "genDefs"
	};

	@Override
	public String getGrammarFileName() { return "JGS.g4"; }

	@Override
	public String[] getTokenNames() { return tokenNames; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public JGSParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}
	public static class JgsContext extends ParserRuleContext {
		public PackageDefContext packageDef() {
			return getRuleContext(PackageDefContext.class,0);
		}
		public GenDefsContext genDefs() {
			return getRuleContext(GenDefsContext.class,0);
		}
		public AlgoDefContext algoDef() {
			return getRuleContext(AlgoDefContext.class,0);
		}
		public PropDefsContext propDefs() {
			return getRuleContext(PropDefsContext.class,0);
		}
		public EntityDefContext entityDef() {
			return getRuleContext(EntityDefContext.class,0);
		}
		public EvalDefsContext evalDefs() {
			return getRuleContext(EvalDefsContext.class,0);
		}
		public JgsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_jgs; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof JGSListener ) ((JGSListener)listener).enterJgs(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof JGSListener ) ((JGSListener)listener).exitJgs(this);
		}
	}

	public final JgsContext jgs() throws RecognitionException {
		JgsContext _localctx = new JgsContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_jgs);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(15);
			_la = _input.LA(1);
			if (_la==1) {
				{
				setState(14); packageDef();
				}
			}

			setState(17); algoDef();
			setState(18); entityDef();
			setState(19); propDefs();
			setState(20); evalDefs();
			setState(21); genDefs();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PackageDefContext extends ParserRuleContext {
		public TerminalNode PackageName() { return getToken(JGSParser.PackageName, 0); }
		public PackageDefContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_packageDef; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof JGSListener ) ((JGSListener)listener).enterPackageDef(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof JGSListener ) ((JGSListener)listener).exitPackageDef(this);
		}
	}

	public final PackageDefContext packageDef() throws RecognitionException {
		PackageDefContext _localctx = new PackageDefContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_packageDef);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(23); match(1);
			setState(24); match(6);
			setState(25); match(PackageName);
			setState(26); match(7);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AlgoDefContext extends ParserRuleContext {
		public TerminalNode Name() { return getToken(JGSParser.Name, 0); }
		public AlgoDefContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_algoDef; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof JGSListener ) ((JGSListener)listener).enterAlgoDef(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof JGSListener ) ((JGSListener)listener).exitAlgoDef(this);
		}
	}

	public final AlgoDefContext algoDef() throws RecognitionException {
		AlgoDefContext _localctx = new AlgoDefContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_algoDef);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(28); match(3);
			setState(29); match(6);
			setState(30); match(Name);
			setState(31); match(7);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class EntityDefContext extends ParserRuleContext {
		public TerminalNode Name() { return getToken(JGSParser.Name, 0); }
		public EntityDefContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_entityDef; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof JGSListener ) ((JGSListener)listener).enterEntityDef(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof JGSListener ) ((JGSListener)listener).exitEntityDef(this);
		}
	}

	public final EntityDefContext entityDef() throws RecognitionException {
		EntityDefContext _localctx = new EntityDefContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_entityDef);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(33); match(8);
			setState(34); match(6);
			setState(35); match(Name);
			setState(36); match(7);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PropDefsContext extends ParserRuleContext {
		public TerminalNode Name(int i) {
			return getToken(JGSParser.Name, i);
		}
		public List<TerminalNode> Name() { return getTokens(JGSParser.Name); }
		public PropDefsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_propDefs; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof JGSListener ) ((JGSListener)listener).enterPropDefs(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof JGSListener ) ((JGSListener)listener).exitPropDefs(this);
		}
	}

	public final PropDefsContext propDefs() throws RecognitionException {
		PropDefsContext _localctx = new PropDefsContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_propDefs);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(38); match(9);
			setState(39); match(5);
			setState(43);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==Name) {
				{
				{
				setState(40); match(Name);
				}
				}
				setState(45);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(46); match(7);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class EvalDefsContext extends ParserRuleContext {
		public TerminalNode Name(int i) {
			return getToken(JGSParser.Name, i);
		}
		public List<TerminalNode> Name() { return getTokens(JGSParser.Name); }
		public EvalDefsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_evalDefs; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof JGSListener ) ((JGSListener)listener).enterEvalDefs(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof JGSListener ) ((JGSListener)listener).exitEvalDefs(this);
		}
	}

	public final EvalDefsContext evalDefs() throws RecognitionException {
		EvalDefsContext _localctx = new EvalDefsContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_evalDefs);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(48); match(4);
			setState(49); match(5);
			setState(51); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				{
				setState(50); match(Name);
				}
				}
				setState(53); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( _la==Name );
			setState(55); match(7);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class GenDefsContext extends ParserRuleContext {
		public TerminalNode Name(int i) {
			return getToken(JGSParser.Name, i);
		}
		public List<TerminalNode> Name() { return getTokens(JGSParser.Name); }
		public GenDefsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_genDefs; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof JGSListener ) ((JGSListener)listener).enterGenDefs(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof JGSListener ) ((JGSListener)listener).exitGenDefs(this);
		}
	}

	public final GenDefsContext genDefs() throws RecognitionException {
		GenDefsContext _localctx = new GenDefsContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_genDefs);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(57); match(2);
			setState(58); match(5);
			setState(60); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				{
				setState(59); match(Name);
				}
				}
				setState(62); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( _la==Name );
			setState(64); match(7);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static final String _serializedATN =
		"\3\u0430\ud6d1\u8206\uad2d\u4417\uaef1\u8d80\uaadd\3\16E\4\2\t\2\4\3\t"+
		"\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\3\2\5\2\22\n\2\3\2\3\2\3\2"+
		"\3\2\3\2\3\2\3\3\3\3\3\3\3\3\3\3\3\4\3\4\3\4\3\4\3\4\3\5\3\5\3\5\3\5\3"+
		"\5\3\6\3\6\3\6\7\6,\n\6\f\6\16\6/\13\6\3\6\3\6\3\7\3\7\3\7\6\7\66\n\7"+
		"\r\7\16\7\67\3\7\3\7\3\b\3\b\3\b\6\b?\n\b\r\b\16\b@\3\b\3\b\3\b\2\2\t"+
		"\2\4\6\b\n\f\16\2\2A\2\21\3\2\2\2\4\31\3\2\2\2\6\36\3\2\2\2\b#\3\2\2\2"+
		"\n(\3\2\2\2\f\62\3\2\2\2\16;\3\2\2\2\20\22\5\4\3\2\21\20\3\2\2\2\21\22"+
		"\3\2\2\2\22\23\3\2\2\2\23\24\5\6\4\2\24\25\5\b\5\2\25\26\5\n\6\2\26\27"+
		"\5\f\7\2\27\30\5\16\b\2\30\3\3\2\2\2\31\32\7\3\2\2\32\33\7\b\2\2\33\34"+
		"\7\r\2\2\34\35\7\t\2\2\35\5\3\2\2\2\36\37\7\5\2\2\37 \7\b\2\2 !\7\f\2"+
		"\2!\"\7\t\2\2\"\7\3\2\2\2#$\7\n\2\2$%\7\b\2\2%&\7\f\2\2&\'\7\t\2\2\'\t"+
		"\3\2\2\2()\7\13\2\2)-\7\7\2\2*,\7\f\2\2+*\3\2\2\2,/\3\2\2\2-+\3\2\2\2"+
		"-.\3\2\2\2.\60\3\2\2\2/-\3\2\2\2\60\61\7\t\2\2\61\13\3\2\2\2\62\63\7\6"+
		"\2\2\63\65\7\7\2\2\64\66\7\f\2\2\65\64\3\2\2\2\66\67\3\2\2\2\67\65\3\2"+
		"\2\2\678\3\2\2\289\3\2\2\29:\7\t\2\2:\r\3\2\2\2;<\7\4\2\2<>\7\7\2\2=?"+
		"\7\f\2\2>=\3\2\2\2?@\3\2\2\2@>\3\2\2\2@A\3\2\2\2AB\3\2\2\2BC\7\t\2\2C"+
		"\17\3\2\2\2\6\21-\67@";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}