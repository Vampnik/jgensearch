package ee.ut.cs.ojandu.jgensearch;

import java.util.Set;

public interface Statement<E extends Entity> {
	public void call(Set<E> set);
}
