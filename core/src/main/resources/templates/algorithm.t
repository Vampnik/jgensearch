$PACKAGE$

import java.util.Set;

import ee.ut.cs.ojandu.jgensearch.Algorithm;
import ee.ut.cs.ojandu.jgensearch.Statement;

public class $ALGORITHM_NAME$ extends Algorithm<$ENTITY_NAME$> {
	
	@SuppressWarnings({ "unchecked", "static-access" })
	@Override
	public Statement<$ENTITY_NAME$> root() {
		// TODO build algorithm
		return null;
	}

	@Override
	public void processResult(Set<$ENTITY_NAME$> result) {
		// TODO process result after algorithm has finished
	}
	
	public static void main(String[] args) {
		run($ALGORITHM_NAME$.class, $ENTITY_NAME$.class, $PROPS_FILE$);
	}

}
