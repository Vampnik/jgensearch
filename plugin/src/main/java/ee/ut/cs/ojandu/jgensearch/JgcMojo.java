package ee.ut.cs.ojandu.jgensearch;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.List;

import org.apache.maven.artifact.DependencyResolutionRequiredException;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.plugins.annotations.ResolutionScope;
import org.apache.maven.project.MavenProject;

import ee.ut.cs.ojandu.jgensearch.util.JGSUtil;

@Mojo(name = "run", requiresDependencyResolution = ResolutionScope.RUNTIME)
public class JgcMojo extends AbstractMojo {

	@Parameter(defaultValue = "${project}")
	private MavenProject project;

	@SuppressWarnings("unchecked")
	@Override
	public void execute() throws MojoExecutionException, MojoFailureException {
		ClassLoader loader = loadDependencies();

		File algNameFile = new File(JGSUtil.JGS_ALG_FILE_NAME);
		if (algNameFile.isDirectory() || !algNameFile.exists()) {
			throw new MojoFailureException(
					"Could not determine JGS algorithm name: "
							+ JGSUtil.JGS_ALG_FILE_NAME
							+ " is either directory or does not exist");
		}

		String algName;
		String entiyName;
		try {
			BufferedReader fin = new BufferedReader(new FileReader(algNameFile));
			algName = fin.readLine();
			entiyName = fin.readLine();
			fin.close();
		} catch (IOException e) {
			throw new MojoExecutionException(
					"Could not determine JGS algorithm name: could not read "
							+ JGSUtil.JGS_ALG_FILE_NAME);
		}

		if (algName == null) {
			throw new MojoExecutionException(
					"Could not determine JGS algorithm name: class name not specified in "
							+ JGSUtil.JGS_ALG_FILE_NAME);
		}

		if (entiyName == null) {
			throw new MojoExecutionException(
					"Could not determine JGS entity name: class name not specified in "
							+ JGSUtil.JGS_ALG_FILE_NAME);
		}

		Class<?> cAlgRaw;
		try {
			cAlgRaw = loader.loadClass(algName);
		} catch (ClassNotFoundException e) {
			throw new MojoFailureException("Could not find algorithm class: "
					+ algName);
		}

		Class<?> cEntityRaw;
		;
		try {
			cEntityRaw = loader.loadClass(entiyName);
		} catch (ClassNotFoundException e) {
			throw new MojoFailureException("Could not find enity class: "
					+ entiyName);
		}

		if (cAlgRaw.isAssignableFrom(Algorithm.class)) {
			throw new MojoFailureException(algName
					+ " is not an algorithm class");
		}

		Class<Algorithm<? extends Entity>> cAlg = (Class<Algorithm<?>>) cAlgRaw;
		Class<Entity> cEnClass = (Class<Entity>) cEntityRaw;

		Algorithm.run(cAlg, cEnClass, JGSUtil.PROPS_FILE);
	}

	@SuppressWarnings("rawtypes")
	private ClassLoader loadDependencies() throws MojoExecutionException {
		try {
			List runtimeClasspathElements = project
					.getRuntimeClasspathElements();
			URL[] runtimeUrls = new URL[runtimeClasspathElements.size()];
			for (int i = 0; i < runtimeClasspathElements.size(); i++) {
				String element = (String) runtimeClasspathElements.get(i);
				runtimeUrls[i] = new File(element).toURI().toURL();
			}
			return new URLClassLoader(runtimeUrls, Thread
					.currentThread().getContextClassLoader());
		} catch (DependencyResolutionRequiredException e) {
			throw new MojoExecutionException("Unable to laod dependecies: " + e.getMessage(), e);
		} catch (MalformedURLException e) {
			throw new MojoExecutionException("Unable to laod dependecies: " + e.getMessage(), e);
		}
	}

}
