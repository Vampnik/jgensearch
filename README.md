JGenSearch
==========

JGenSearch aims to define and implement viable grammar in order to make experimenting 
with genetic search algorithms more convenient. Genetic search algorithms are family 
of heuristic search algorithms that borrow their pattern from nature. Namely they try 
to mimic evolution and natural selection. Genetic algorithm are usually used when the 
search space is to large to use a more greedy approach. Its pattern in based on a population 
of entities initially usually generated with random values or properties. For each algorithm 
there is function defined for evaluating the population and its entities. After this subset 
of entities deemed more suitable considering the evaluation, they are selected and used for 
generating new population. This process is repeated either for defined n-times or until a 
satisfactory result has been achieved.

Most of the genetic algorithms the pattern is the same as above demonstrated but there can be 
variations. JGenSearch aims to create a set of sub-algorithms mainly for managing the 
population of entities. JGenSearch generates stubs that have to be filled in with algorithm 
specific entity definitions. Sub-algorithms can be tied together using JGenSearch Embedded DSL.

## Setup

* Clone the source
* In the source directory run:
```
mvn clean install
```
* Unpack container/target/jgensearch-container-$VERSION-dist.zip to desired location
* Define environment variables:
```
export JGS_HOME=/path/to/jgs/distribution
export PATH=$PATH:$JGS_HOME
```

## Generating JGS project

* Make a directory for a new project
* Create a JGS file inside the directory. For example "sample.jgs":
```
package=ee.ut.cs.jgstest;
name=TestAlgorithm;
entity=TestEntity;
Props : top timesRepeat;
Evals : evaluationFunction;
Gens : generateFromPair
generateFromAll;
```
* Run:
```
jgs-init sample.jgs
```


## Defining algorithm

Abovementioned process will create a Maven project which contains stub classes that extend
Algorithm.class and Entity.class. 

* Define initial generation logic in the Entity sub-class
* Fill in evaluation function stubs in the Entity sub-class
* Fill in new population generation stubs in the Entity sub-class
* Fill in the population handling algorithm using JGS Embedded DSL in the Algorithm sub-class. For example: 
```
return block(
	initialGenerate(prop("top")*prop("top")),
	repeat(prop("timesRepeat"), 
		each(entity().evaluationFunction),
		keepTop(prop("top")),
		allPairs(entity().generateFromPair)
	)
);
```
* Define post process actions that will be taken after search algorithm finished in the
Algorithm sub-class. 
* Configure properties used 

## Running the algorithm

Either:

* use jgs-maven-plugin by running:
```
mvn jgs:run
```
* JGenSearch project initialization generates a main method for the Algorithm sub-class
* Algorithm class has a static run method
* Inside code. For example:
```
new TestAlgorithm().root().call(entitySet);
```